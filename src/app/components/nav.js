import React, { Component } from 'react';

class Nav extends Component {
  constructor() {
    super();

    this.onKeyDown = this.onKeyDown.bind(this);
  }

  componentDidMount() {
    // console.log(this.props);
  }

  onKeyDown(e) {
    const { target } = e;

    if (e.key === 'Enter' && target.value.length > 0) {
      console.log('go...');
      this.props.execSearch(target.value);
    }

    if (target.value.length < 3) {
      console.log('nope...');
    }
  }

  render() {
    return (
      <div className="navbar-fixed">
        <nav className=" grey darken-4">
          <div className="nav-wrapper">
            <form onSubmit={(e) => { e.preventDefault(); }}>
              <div className="input-field">
                <input id="search" type="search" onKeyDown={this.onKeyDown} required />
                <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
                <i className="material-icons">close</i>
              </div>
            </form>
          </div>
        </nav>
      </div>
    );
  }
}

export default Nav;
