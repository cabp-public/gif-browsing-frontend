class ContentController {
  constructor(container, options) {
    this._container = container;
    this._options = options;
  }

  cleanResults() {
  }

  getPage(page) {
  }

  onSubmit(e) {
  }

  onPageClick(event) {
  }
}

export default ContentController;
