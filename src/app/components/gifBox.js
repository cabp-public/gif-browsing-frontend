import React, { Component } from 'react';

class GifBox extends Component {
  constructor() {
    super();

    this.state = {
      apiUrl: 'http://gif-browser-api.local',
      item: false
    };
  }

  componentDidMount() {
    this.setState({ item: this.props.item });
  }

  componentWillReceiveProps(nextProps) {
    // console.log('New Props!');
    // console.log(results);

    // this.setState({ results: results.results });
  }

  render() {
    let { item } = this.state;

    return item !== false ? (
      <div className="col s12 m4">
        <div className="card">
          <div className="card-image">
            <img src={this.state.apiUrl + item.viewUrl} />
          </div>
          <div className="card-content">
            {item.tags.map(tag => {
                return (
                  <div key={ tag } className="chip">{ tag }</div>
                );
              }
            )}
          </div>
          <div className="card-action">
            <a href="#">This is a link</a>
          </div>
        </div>
      </div>
    ) : null;
  }
}

export default GifBox;
