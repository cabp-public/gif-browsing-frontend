import React, { Component } from 'react';

import GifBox from './gifBox';

class Results extends Component {
  constructor() {
    super();

    this.state = {
      totalPages: 1,
      currentPage: 1,
      results: []
    };
  }

  componentDidMount() {
  }

  componentWillReceiveProps(newProps) {
    this.setState({ totalPages: newProps.totalPages });
    this.setState({ currentPage: newProps.currentPage });
    this.setState({ results: newProps.results });
  }

  loadPage(page) {
     this.props.execSearch(this.props.keywords.join(','), page);
  }

  renderPages(totalPages) {
    const { currentPage } = this.state;
    const pages = [];
    let cl;

    for (let p = 1; p <= totalPages; p++) {
      cl = p === currentPage ? 'chip cursor-pointer orange' : 'chip cursor-pointer';

      pages.push(
        <div key={ p } onClick={(e) => { this.loadPage(p) }}
          className={cl}>{ p }
        </div>
      );
    }

    return pages;
  }

  render() {
    const results = this.state.results;
    const totalPages = this.state.totalPages;

    return (
      <div className="container">
        <div className="row">
          {results.map(item => {
              return (
                <GifBox key={ item.key } item={ item } />
              );
            }
          )}
        </div>

        <div className="row">
          <div className="col s12 center-align">
            { this.renderPages(totalPages) }
          </div>
        </div>
      </div>
    );
  }
}

export default Results;
