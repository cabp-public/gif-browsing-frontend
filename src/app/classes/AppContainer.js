import * as appStructure from '../../config/appStructure';

// Services
import HttpService from '../services/HttpService';

// Controllers
import ContentController from './controllers/ContentController';

const KEY_CONTROLLERS = 'controllers';
const KEY_SERVICES = 'services';

class AppContainer {
  constructor() {
    /*if (!!AppContainer.instance) {
      console.log('Returning instantiated container');
      return AppContainer.instance;
    }*/

    console.log('Building container...');
    // AppContainer.instance = this;

    this.initOptions = {};

    Object.keys(appStructure).forEach((key) => {
      this[key] = {};
      this.initOptions[key] = appStructure[key];
    });
  }

  start(delay = 2000) {
    const initEventReady = new CustomEvent(AppContainer.initEvent);
    const serviceInitOpts = this.initOptions[KEY_SERVICES];
    const ctrlInitOpts = this.initOptions[KEY_CONTROLLERS];

    // Services
    const servicesPool = {
      Http: new HttpService(this, serviceInitOpts['Http'] || {})
    };

    Object.keys(servicesPool).forEach((key) => this[KEY_SERVICES][key] = servicesPool[key]);

    // Controllers
    const controllersPool = {
      ContentController: new ContentController(this, ctrlInitOpts['Content'] || {})
    };

    Object.keys(controllersPool).forEach((key) => this[KEY_CONTROLLERS][key] = controllersPool[key]);

    // Let's give it a second :V
    setTimeout(() => {
      document.dispatchEvent(initEventReady);
    }, delay);
  }

  clearGlobalLoader() {
    let el = document.getElementsByClassName('globalLoader')[0];
    el.className = 'nada';
  }
}

AppContainer.initEvent = 'INIT_READY';

export default AppContainer;
