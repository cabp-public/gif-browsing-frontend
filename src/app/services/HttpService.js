class HttpService {
  constructor(container, options) {
    this._container = container;
    this._options = options;

    this.get = this.get.bind(this);
    this.post = this.post.bind(this);
  }

  get(url, callback) {
    let fullUrl = this._options['protocol'] + '://' + this._options['host'];
    fullUrl += this._options['basePath'] + url;

    fetch(fullUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  post(url, callback, data) {
    let fullUrl = this._options['protocol'] + '://' + this._options['host'];
    fullUrl += this._options['basePath'] + url;

    fetch(fullUrl, {
      method: 'POST',
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data)
      }
    })
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  put() {}

  delete() {}
}

export default HttpService;
