import React, { Component } from 'react';

import AppContainer from '../index';

class History extends Component {
  constructor() {
    super();

    this.state = {
      currentPage: 0,
      totalPages: 1,
      pageResults: []
    };

    this.onResults = this.onResults.bind(this);
    this.loadPage = this.loadPage.bind(this);
  }

  componentDidMount() {
    this.loadPage(0);
  }

  componentWillReceiveProps(newProps) {
  }

  loadPage(page) {
    AppContainer.services.Http.get('history/' + page, this.onResults, {});
  }

  renderPages(totalPages) {
    const { currentPage } = this.state;
    const pages = [];
    let cl;

    for (let p = 0; p < totalPages; p++) {
      cl = p === currentPage ? 'chip cursor-pointer orange' : 'chip cursor-pointer';

      pages.push(
        <div key={ p } onClick={(e) => { this.loadPage(p) }}
          className={cl}>{ p + 1 }
        </div>
      );
    }

    return pages;
  }

  onResults(data) {
    const { currentPage, totalPages, pageResults } = data.data;
    this.setState({ currentPage, totalPages, pageResults });
  }

  render() {
    const { currentPage, totalPages, pageResults } = this.state;

    return (
      <div className="container">
        <div className="row">
          <table>
            <thead>
              <tr>
                  <th>Date</th>
                  <th>Keywords</th>
              </tr>
            </thead>

            <tbody>
              {pageResults.map(item => {
                  return (
                    <tr key={ item.his_created_at }>
                      <td>{ item.his_created_at }</td>
                      <td>{ item.his_search_string }</td>
                    </tr>
                  );
                }
              )}
            </tbody>
          </table>
        </div>

        <div className="row">
          <div className="col s12 center-align">
            { this.renderPages(totalPages) }
          </div>
        </div>
      </div>
    );
  }
}

export default History;
