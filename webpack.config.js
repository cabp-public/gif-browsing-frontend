const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html'
});

const copyPlugin = new CopyWebpackPlugin([
  {
    from: './src/assets/**/*.+(jpg|png)',
    to: './assets/[name].[ext]',
    toType: 'template'
  }
]);

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: {
          loader: 'url-loader'
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2|wasm)$/,
        use: {
          loader: 'file-loader'
        }
      }
    ]
  },
  devServer: {
    historyApiFallback: true
  },
  plugins: [htmlPlugin, copyPlugin],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve('../', 'BlueCodingFrontDist')
  }
};