import React from 'react';
import { render } from 'react-dom';
import './assets/css/materialize.min.css';
import './assets/css/custom.css';

import AppContainer from './app/';
import App from './app/app';

// const { constructor: { initEvent } } = AppContainer;

document.addEventListener('INIT_READY', function (event) {
  console.log('Ready to render...');

  AppContainer.clearGlobalLoader();
  render(<App />, document.getElementById('root'));
});

AppContainer.start();
