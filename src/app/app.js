import React, { Component } from 'react';

import AppContainer from './index';
// import AppContainer from './classes/AppContainer';
import Nav from './components/nav';
import Results from './components/results';
import History from './components/history';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      moduleReady: false,
      historyMode: false,
      keywords: [],
      totalPages: 1,
      totalResults: 0,
      currentPage: 1,
      results: []
    };

    this.execSearch = this.execSearch.bind(this);
    this.toggleHistory = this.toggleHistory.bind(this);
    this.onResults = this.onResults.bind(this);
  }

  componentDidMount() {
    const moduleReady = true;

    this.setState({ moduleReady });
  }

  execSearch(words, page = 1) {
    const searchData = {
    	keywords: words.split(' '),
    	detailed: 0,
    	page: page,
    	pageLimit: 6
    };

    this.setState({ keywords: searchData.keywords });
    AppContainer.services.Http.post('search', this.onResults, searchData);
  }

  toggleHistory(e) {
    const { historyMode } = this.state;
    this.setState({ historyMode: !historyMode });

    e.preventDefault();
  }

  onResults(data) {
    const { totalPages, totalResults, currentPage, results } = data.data;
    this.setState({ totalPages, totalResults, currentPage, results });
  }

  render() {
    const { moduleReady, historyMode } = this.state;
    const keywords = this.state.keywords;
    const totalPages = this.state.totalPages;
    const currentPage = this.state.currentPage;
    const results = this.state.results;

    if (moduleReady === true && historyMode === false) {
      return (
        <div>
          <Nav execSearch={this.execSearch} />
          <Results
            execSearch={this.execSearch}
            keywords={keywords}
            totalPages={totalPages}
            currentPage={currentPage}
            results={results}
          />

          <div className="fixed-action-btn">
            <a onClick={this.toggleHistory} className="btn-floating btn-large red">
              <i className="large material-icons">history</i>
            </a>
          </div>
        </div>
      );
    }
    else if (moduleReady === true && historyMode === true) {
      return (
        <div>
          <History />

          <div className="fixed-action-btn">
            <a onClick={this.toggleHistory} className="btn-floating btn-large red">
              <i className="large material-icons">search</i>
            </a>
          </div>
        </div>
      );
    }
    else {
      return null;
    }
  }
}

export default App;
